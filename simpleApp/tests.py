from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from .views import index
from .models import Status
from .forms import StatusForm

class Lab6UnitTest(TestCase):
    def test_lab6_url_exists(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_lab6_using_index_function(self):
        foundFunc = resolve('/')
        self.assertEqual(foundFunc.func, index)
    
    def test_model_can_create_objects(self):
        status = Status.objects.create(status='Lagi mau ngetest')
        self.assertIsInstance(status, Status)
    
    def test_form_validation_for_blank_fields(self):
        form = StatusForm(data={'status':''})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['status'], ["This field is required."])
    
    def test_form_validation_for_correct_fields(self):
        form = StatusForm(data={'status':'Yahoo!'})
        self.assertTrue(form.is_valid())
    
    def test_lab6_post_success_and_render_the_result(self):
        response_post = Client().post('/', {'status':'WOOHOO!'})
        self.assertEqual(response_post.status_code, 302)

        response = Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertIn('WOOHOO!', html_response)
    
    def test_lab6_post_failed_and_render_the_result(self):
        response_post = Client().post('/', {'status':''})
        self.assertEqual(response_post.status_code, 200)

        response = Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertNotIn('WOOHOO!', html_response)
    
class Lab6FunctionalTest(LiveServerTestCase):
    def setUp(self):
        self.selenium  = webdriver.Firefox()
        super(Lab6FunctionalTest, self).setUp()
    
    def tearDown(self):
        self.selenium.quit()
        super(Lab6FunctionalTest, self).tearDown()
    
    def test_form_input(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')

        status = selenium.find_element_by_id('id_status')
        submit = selenium.find_element_by_id('submit')

        status.send_keys('Coba Coba')

        submit.send_keys(Keys.RETURN)

        selenium.get('http://127.0.0.1:8000/')
        assert 'Coba Coba' in selenium.page_source